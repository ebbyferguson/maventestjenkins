package restproj;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

//the_pat_im_building
//host:8000/restproj/v1/resource

@ApplicationPath("v1")
public class AppConfig extends Application {

	private Set<Class<?>> resources = new HashSet<>();
	
	public AppConfig(){
		System.out.println("Created AppConfig");
		resources.add(R1.class);
	}

	@Override
	public Set<Class<?>> getClasses() {

		return resources;
	}

}
